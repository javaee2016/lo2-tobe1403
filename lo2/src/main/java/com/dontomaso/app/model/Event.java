package com.dontomaso.app.model;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by tomasoberg on 2016-11-08.
 */

@Entity
@Table(name = "EVENTS")
public class Event implements Serializable {

    /**
     * Protected variabels
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String city;
    private String description;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private LocalDateTime lastUpdate;

    //-------------------------------------------------------------------------
    // Pekar på Comment.event
    //-------------------------------------------------------------------------
    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private List<Comment> comments;

    //-------------------------------------------------------------------------
    // Pekar på User.event
    // Skapar en knyttabell vid namn organizers med de två kolumnerna EVENT_ID och USER_ID
    //-------------------------------------------------------------------------
    @ManyToMany( cascade = { CascadeType.PERSIST,  CascadeType.MERGE })
    @JoinTable( name = "ORGANIZERS",
        joinColumns         = @JoinColumn(name = "EVENT_ID"),
        inverseJoinColumns  = @JoinColumn(name = "USER_ID")
    )
    private List<User> users;



    /**
     * Getters
     */
    public long getId() {
        return id;
    }
    public List<Comment> getComments() {
        return comments;
    }


    public List<User> getUsers()            {   return users;  }

    public String getTitle() {
        return title;
    }
    public String getCity() {
        return city;
    }
    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Setters
     */
    public void setUsers(List<User> users)  {
        this.users = users;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
    public void setDescription(String description) { this.description = description; }

    /**
     * Constructors
     */
    public Event() {
        comments = new ArrayList<>();
    }


    // TODO: Lägg till sedan, kolla om du behöver initializera users och comments
    public Event(String title, String city, String description, LocalDateTime startTime, LocalDateTime endTime) {
        this.title = title;
        this.city = city;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.lastUpdate = LocalDateTime.now();
        this.users = new ArrayList<>();
        this.comments =  new ArrayList<>();


    }

    public Event(String title, String city, String description, LocalDateTime startTime, LocalDateTime endTime, List<Comment> comments, List<User> users) {
        this.title = title;
        this.city = city;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.users = users;
        this.comments = comments;
        this.lastUpdate = LocalDateTime.now();
    }


    /**
     * Lägg till en kommentar till eventet
     * @param comment
     * @return
     */
    public boolean addComment(Comment comment) {
        if(!comments.contains(comment) && comments.add(comment)) {
            lastUpdate = LocalDateTime.now();
            return true;
        }

        return false;
    }

    /**
     * Lägger till en användare till eventet
     * @param user
     * @return
     */

    public boolean addUser(User user) {
        if(!users.contains(user) && users.add(user)) {
            lastUpdate = LocalDateTime.now();
            return true;
        }
        return false;
    }




}
