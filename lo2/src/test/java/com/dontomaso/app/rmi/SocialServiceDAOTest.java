package com.dontomaso.app.rmi;

import com.dontomaso.app.model.Comment;
import com.dontomaso.app.model.Event;
import com.dontomaso.app.model.User;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomasoberg on 2016-11-23.
 */
public class SocialServiceDAOTest extends TestCase {
    SocialServiceInterface comp;

    @Before
    public void setUp() throws RemoteException  {
        String name = "jee16";
        Registry registry = LocateRegistry.getRegistry("localhost");
        try {
            comp = (SocialServiceInterface) registry.lookup(name);


        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
    @After
    public void tearDown() throws RemoteException {
        //comp.shutDown();
    }

    @Test
    public void testCountAllUsers() throws RemoteException {
        try {
            List<User> users = (ArrayList<User>) comp.queryJPQL("SELECT u FROM User u");
            assertEquals(7, users.size());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCountAllEvents() throws RemoteException {
        try {
            List<Event> events = (ArrayList<Event>) comp.queryJPQL("SELECT e FROM Event e");
            assertEquals(8, events.size());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test
    public void testCountAllComments() throws RemoteException {
        try {
            List<Comment> comments = (ArrayList<Comment>) comp.queryJPQL("SELECT c FROM Comment c");
            assertEquals(12, comments.size());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testChangeAndMerge() throws  RemoteException {
        try {

            //-----------------------------------------------------------------
            // Try and retrieve an existing user and assert that we have
            // retrieved an user object
            //-----------------------------------------------------------------
            User usr = (User) comp.simpleQueryJPQL("SELECT u FROM User u WHERE u.firstName='Fredrik'");
            assertNotNull(usr);

            //-----------------------------------------------------------------
            // Change the name of the detached object and update (merge) the
            // changed user back
            //-----------------------------------------------------------------
            usr.setFirstName("Anders");
            comp.update(usr);

            //-----------------------------------------------------------------
            // Assert that the name has been changed
            //-----------------------------------------------------------------
            usr = (User) comp.simpleQueryJPQL("SELECT u FROM User u WHERE u.firstName='Anders'");
            assertEquals("Anders", usr.getFirstName() );

            //-----------------------------------------------------------------
            // Change back the name and update the object
            //-----------------------------------------------------------------
            usr.setFirstName("Fredrik");
            comp.update(usr);

            //-----------------------------------------------------------------
            // Assert - we will now check that the name is back to normal
            //-----------------------------------------------------------------
            usr = (User) comp.simpleQueryJPQL("SELECT u FROM User u WHERE u.firstName='Fredrik'");
            assertEquals("Fredrik", usr.getFirstName() );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSQL() throws  RemoteException {
        try {
            List<User> users = (List<User>) comp.querySQL("SELECT * FROM USERS");
            assertEquals(7, users.size());

            // Just check that this doesn't throw any exceptions
            comp.executeSQL("SELECT * FROM EVENTS");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testAddingAndRemovingUser() throws  RemoteException {
        try {

            //-----------------------------------------------------------------
            // Add a new user
            //-----------------------------------------------------------------
            User user = new User("testur@testursson.com", "testur", "testursson");
            comp.create(user);

            //-----------------------------------------------------------------
            // Try and retrieve it with either of these methods:
            //-----------------------------------------------------------------
            //User usr = (User) comp.simpleQueryJPQL("SELECT u FROM User u WHERE u.firstName='testur'");
            //ArrayList<User> usrs = (ArrayList<User>) comp.runNamedQuery("findUserByEmail","email","testur@testursson.com");
            //ArrayList<User> usrs = (ArrayList<User>) comp.runNamedQuery("findUserByFirstName","fName","testur");
            ArrayList<User> usrs = (ArrayList<User>) comp.runNamedQuery("findUserByLastName","lName","testursson");

            //-----------------------------------------------------------------
            // Assert - we should have retrieved an user object
            //-----------------------------------------------------------------
            assertNotNull(usrs.get(0));
            assertEquals(usrs.get(0).getFirstName(), "testur");

            //-----------------------------------------------------------------
            // Now delete the user
            //-----------------------------------------------------------------
            comp.delete(usrs.get(0));

            //-----------------------------------------------------------------
            // Assert - we should not get the same result this time
            //-----------------------------------------------------------------
            usrs = (ArrayList<User>) comp.runNamedQuery("findUserByLastName","lName","testursson");
            assertTrue( usrs.isEmpty() );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }




}