package com.dontomaso.app.rmi;

import com.dontomaso.app.model.*;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by tomasoberg on 2016-11-21.
 */
public class SocialServiceFacade implements SocialServiceInterface {
    private SocialServiceDAO dao = new SocialServiceDAO();

    /**
     * Send a jpqlQuery
     * @param jpqlQuery
     * @return
     * @throws RemoteException
     */
    @Override
    public Object queryJPQL(String jpqlQuery) throws RemoteException {
        return dao.queryJPQL(jpqlQuery);
    }

    @Override
    public Object querySQL(String sqlQuery) throws RemoteException {
        return dao.querySQL(sqlQuery);
    }

    @Override
    public void executeSQL(String sqlQuery) throws RemoteException {
        dao.executeSQL(sqlQuery);
    }






    @Override
    public Object simpleQuerySQL(String sqlQuery) throws RemoteException {
        return dao.simpleQuerySQL(sqlQuery);
    }

    @Override
    public Object simpleQueryJPQL(String jpqlQuery) throws RemoteException {
        return dao.simpleQueryJPQL(jpqlQuery);
    }


    /**
     * Shutdown the entitymanagerfactory object
     * @throws RemoteException
     */
    @Override
    public void shutDown() throws RemoteException {
        dao.shutDown();
    }

    @Override
    public void flush() throws RemoteException {
        dao.flush();
    }


    //-------------------------------------------------------------------------
    // CRUD - operations
    //-------------------------------------------------------------------------
    @Override
    public void create(Object obj) throws RemoteException {
        dao.persist(obj);
    }
    @Override
    public Object read(Class cls, Object obj) throws RemoteException {
        return dao.read(cls,obj);
    }
    @Override
    public void update(Object obj) throws RemoteException {
        dao.update(obj);
    }
    @Override
    public void delete(Object obj) throws RemoteException {
        dao.remove(obj);
    }






    public Object runNamedQuery(String queryName, String parameter, String searchText) {
        return dao.runNamedQuery(queryName, parameter, searchText);
    }




    /**
     * Store all the events in the database using JPA
     * @throws RemoteException
     */

    @Override
    public void add(List<Event> events)  throws RemoteException {
        dao.persistAll(events);
    }


}
