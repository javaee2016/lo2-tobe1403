package com.dontomaso.app.rmi;

/**
 * Created by tomasoberg on 2016-11-21.
 */

import com.dontomaso.app.model.Event;
import com.dontomaso.app.model.User;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Interface for SocialServiceInterface
 */
public interface SocialServiceInterface extends Remote {
    Object simpleQuerySQL(String sqlQuery) throws RemoteException;
    Object simpleQueryJPQL(String jpqlQuery) throws RemoteException;
    Object queryJPQL(String jpqlQuery) throws RemoteException;
    Object querySQL(String sqlQuery) throws RemoteException;
    void executeSQL(String sqlQuery) throws RemoteException;
    Object runNamedQuery(String queryName, String parameter, String searchText) throws RemoteException;

    void flush() throws RemoteException;
    void shutDown() throws RemoteException;
    void add(List<Event> events)  throws RemoteException;

    // CRUD ------
    void create(Object obj) throws RemoteException;
    Object read(Class cls, Object obj) throws RemoteException;
    void update(Object obj) throws RemoteException;
    void delete(Object obj) throws RemoteException;


}
