package com.dontomaso.app;

import com.dontomaso.app.rmi.*;


import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by tomasoberg on 2016-11-21.
 */

// TODO: Create separate project folder for SocialServer
public class SocialServer {

    SocialServer() {
        System.getProperties().setProperty("java.security.policy", "no.policy");
        System.getProperties().setProperty("java.rmi.server.hostname", "localhost");

        try {
            java.rmi.registry.LocateRegistry.createRegistry(1099);
            String name = "jee16";
            SocialServiceInterface populator = new SocialServiceFacade();
            SocialServiceInterface stub = (SocialServiceInterface) UnicastRemoteObject.exportObject( populator, 0 );
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("SocialServiceFacade bound");

        }
        catch (Exception e) {
            System.err.println("SocialServiceFacade exception:");
            e.printStackTrace();
        }



    }
}
