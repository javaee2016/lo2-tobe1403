package com.dontomaso.app.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by tomasoberg on 2016-11-08.
 */

@Entity
@Table(name = "USERS")
@NamedQueries(
    {
        @NamedQuery(name = "findUserByEmail", query = "SELECT u FROM User u WHERE u.email=:email"),
        @NamedQuery(name = "findUserByFirstName", query = "SELECT u FROM User u WHERE u.firstName=:fName"),
        @NamedQuery(name = "findUserByLastName", query = "SELECT u FROM User u WHERE u.lastName=:lName")
    }
)
public class User implements Serializable {

    /**
     * Protected variabels
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String lastName;
    private String email;

    //-------------------------------------------------------------------------
    // Pekar på Comment.user
    //-------------------------------------------------------------------------
    @OneToMany(mappedBy = "user", cascade=CascadeType.ALL)
    private Set<Comment> comments;

    public User(User usr) {
        this.email = usr.getEmail();
        this.firstName = usr.getFirstName();
        this.lastName = usr.getLastName();
    }

    /**
     * Getters
     */
    public long getId() {
        return id;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }
    public Set<Comment> getComments() {
        return comments;
    }


    /**
     * Setters
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
    /**
     * Constructors
     */
    public User() { }
    public User(String email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " (" + email + ")";
    }

}
