package com.dontomaso.app.rmi;

/**
 * Created by tomasoberg on 2016-11-21.
 */

import com.dontomaso.app.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

/**
 * A Data Access Object class implementation that hides all database specific code from the caller.
 */
public class SocialServiceDAO {
    private static EntityManagerFactory entityManagerFactory;

    /**
     * Constructor, creates an EntityManagerFactory object
     */
    public SocialServiceDAO() {
        entityManagerFactory = Persistence.createEntityManagerFactory("social");
    }

    /**
     * Shutdown the entityManagerFactory object
     */
    public void shutDown() {
        entityManagerFactory.close();
    }

    /**
     * Send a jppqlQuery
     *
     * @param jpqlQuery
     * @return
     */

    public Object simpleQueryJPQL(String jpqlQuery) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.createQuery(jpqlQuery).getSingleResult();
    }

    public Object queryJPQL(String jpqlQuery) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.createQuery(jpqlQuery).getResultList();
    }


    public Object simpleQuerySQL(String sqlQuery) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.createNativeQuery(sqlQuery).getSingleResult();
    }

    public Object querySQL(String sqlQuery) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.createNativeQuery(sqlQuery).getResultList();
    }


    public void executeSQL(String sqlQuery) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.createNativeQuery(sqlQuery);
        em.close();
    }



    public void flush() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.flush();
        em.close();
    }


    public Object runNamedQuery(String queryName, String parameter, String searchText) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.createNamedQuery(queryName).setParameter(parameter, searchText).getResultList();

    }

    // TODO: This is not working
    public void update(Object obj) {
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(obj);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public Object read(Class cls, Object obj) {
        EntityManager em = entityManagerFactory.createEntityManager();
        return em.getReference(cls, obj);
    }

    public void remove(Object obj) {
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.remove(em.merge(obj));
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void persist(Object obj) {
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.persist(obj);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
    }
    public void persistAll(List<Event> events) {
        EntityManager em = entityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            for (Object obj : events)
                em.persist(obj);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
    }


}