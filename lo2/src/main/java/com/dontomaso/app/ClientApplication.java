package com.dontomaso.app;

import com.dontomaso.app.rmi.*;
import com.dontomaso.app.model.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * ClientApplication which calls through RMI-tech method which parses and all the events
 * found in a text file.
 */
public class ClientApplication {
	public static void main(String[] args) {
        SocialClient client = new SocialClient();
        client.parseAllEvents();
    }
}
